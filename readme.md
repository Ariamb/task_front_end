Opa, tudo bem?


completei tanto a task 1 - html e css puro - quanto a task 2 - usando bootstrap.


Acho que não é nem preciso dizer sobre como a task2 foi mais fácil (e saiu mais bonita) que a task 1.
Tentei seguir o template do figma disponibilizado com o material, mas na task 1 foi complicado. Ela foi bastante demorada, também - e acabou com um arquivo .css cheio de configurações.
A task 2 foi simples, e o arquivo .css acabou sendo apenas algumas configurações ocasionais de padding e margem.


Eu usei o gerador de texto Lorem Ipsum para preencher placeholders.


Quanto a escolha de imagens, tive que usar o que eu já possuia salvo no meu computador - capas de albuns musicais. 
Isso porquê eu estou com um problema de conexão que durou a semana toda, mal conseguindo acessar o git - quem dirá sites de compartilhamento de imagens em alta resolução.
Eu acabei upando duas pastas images diferentes. Fica feio repetir as imagens, mas assim mantém uma estrutura legalzinha.